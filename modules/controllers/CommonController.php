<?php

namespace app\modules\controllers;

use yii\web\Controller;
use Yii;

class CommonController extends Controller
{
    public function init()
    {
        $app_path = Yii::$app->modules['admin']->module->requestedRoute;
        $app_path = explode('/',$app_path);
        $ext_module_admin_controller = ['manage'];
        $ext_module_admin_action = ['mailchangepass'];
        if(!in_array($app_path[1],$ext_module_admin_controller) || !in_array($app_path[2],$ext_module_admin_action)){
            if (Yii::$app->session['admin']['isLogin'] != 1) {
                return $this->redirect(['/admin/public/login']);
            }
        }

    }
}
